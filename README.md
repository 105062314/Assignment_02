# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

* **Gitlab Game URL : https://105062314.gitlab.io/Assignment_02/**
* **Firebase Game URL : https://assignment02-94dd1.firebaseapp.com/**

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>
<img src="example02.png" width="700px" height="500px"></img>

## Scoring 
|                                              Item                                              | Score | check |
|:----------------------------------------------------------------------------------------------:|:-----:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |  yes  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |  yes  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |  yes  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |  yes  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |  yes  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |  yes  |
| Appearance (subjective)                                                                        |  10%  |  yes  |
| Other creative features in your game (describe on README.md)                                   |  10%  |  yes  |

## About My NS-Shaft
* NS-Shaft 小朋友下樓梯
* 主要功能

    1. Menu : 可選擇單人或雙人模式
    2. Menu小動畫，可操作角色移動左右，選單箭頭移動
    3. 操作左右移動角色，角色不斷下降（與小朋友下樓梯同）
    4. 在下降時有許多平台可停留
    5. 平台陷阱：尖刺、輸送帶（左右）、彈簧平台
    6. 頂端有尖刺
    7. 有life*10跟分數計算功能
    8. 判定死亡：life<=0 或掉入底層
    9. 排行榜(firebase)
    10. 重新開始功能
    11. 音效配置（尖刺、彈簧、死亡、背景音樂、選單音樂）

* 特殊功能

    1. 支援手機觸控（僅限單人模式）
    2. 支援滑鼠操作（僅限單人模式）
    3. 根據視窗大小（筆電或手機）來自動設定長寬
    4. loading page 先等待圖面仔入完成後才到menu

## Function Detail
|       **states**      |              handle           |
|:---------------------:|:-----------------------------:|
|       **boot**        |   先載入loadState要用到的東西    |
|       **load**        |   先載入全部圖片，並有進度條      |
|       **menu**        |   判斷進入單人或雙人模式    |
|       **main**        |   單人模式遊戲        |
|      **two_player**   |   雙人模式遊戲        |

## html 部分
* 加入RWD及利用CSS調整視窗大小

## JavaScript
* Basic
    * 每個state都分為 preload create update 
    * preload : 預先跑好所需載入之物件（ex:圖片、音檔）
    * create : 設定物件特性
    * update : 隨時更新頁面資訊
* Physics
    * game.physics.startSystem(Phaser.Physics.ARCADE);
    * 利用上行載入物理引擎，即可達成碰撞，墜落等物理特性
* 載入圖片Sprite
    * 設定背景及其他固定之物件
* Spritesheet
    * 一般載入圖片為Sprite，但利用Spritesheet可利用一張圖片設定動畫
    * 利用 animations.add設定動畫的運作方式
* 鍵盤、觸控及滑鼠
    * createCursorKeys() 此function能包含上下左右等方向鍵
    * 自訂keyboard（game.input.keyboard.addKeys）可設定所需要之按鍵
    * game.input.addPointer(); 加入觸控點（可設置多個）
    * game.input.mousePointer 滑鼠點擊操作
* 角色
    * 判斷玩家目前按下之按鍵，更改角色的速度
    * 預設重力為500 自動下墜
    * 判斷碰撞 physics.arcade.collide 
* 平台
    * 根據random變數隨機產生平台，並先存入陣列中
    * 每0.6秒將陣列中的平台顯示出來，並將其y座標不斷向上移動
    * 當角色與平台碰撞時，產生不同特效（在effect中設定）
* 音樂
    * game.load.audio載入音樂
    * loop 設定重複播放 volume設定大小聲
    * play() stop()進行播放及停止
* 排行榜
    * 在結束時，將玩家成績上傳至firebase上
    * 利用midterm project學到之技巧，讀出前三高的名次
    * 存入時我利用負數，這樣存取時較為方便

## Reference
* https://phaser.io/examples/v2/input/virtual-gamecontroller/
* https://phaser.io/examples/v2/input/multi-touch/
* http://www.html5gamedevs.com/topic/21639-phaser-scale-manager-show-all-not-working/