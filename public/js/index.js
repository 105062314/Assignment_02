var score = 0;
var score2 = 0;
var last_shaft_time = 0;
var shafts = [];
var status = 'running';
var ranking = [];
var keyboard;
var jumpSound;
var hitSound;
var dieSound;
var menu_musicSound;
//var bgm;

var bootState = { 
    preload: function () {
    // Load the progress bar image.
        game.load.image('progressBar', 'assets/progressBar.png');
        game.load.audio('menu_music', ['assets/sounds/menu_music.wav']);
        game.load.image('menu','./assets/menu.png');
    },
    create: function() {
        menu_musicSound = game.add.audio('menu_music');
        menu_musicSound.loop = true;
        menu_musicSound.volume = 0.3;
        game.stage.backgroundColor = '#3498db';
       // game.add.tileSprite(0, 0, 2000,2000, 'menu').scale.setTo(0.5,0.5);
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.state.start('load');
    } 
};

var loadState = {
    preload:function(){

        //game.load.image('progressBar','./assets/progressBar.png');
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,'loading...', { font: '30px Arial', fill: '#ffffff' }); 
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar'); 
        progressBar.scale.setTo(1.5,0.4);
        progressBar.anchor.setTo(0.5, 0.5); 
        game.load.setPreloadSprite(progressBar);

        game.load.image('shaft','./assets/shaft.png');
        game.load.image('spiking','./assets/spiking.png');
        game.load.image('ceiling','./assets/ceiling.png');
        game.load.image('wall','./assets/wall.png');
        game.load.image('tri','./assets/tri.png');
        game.load.image('background','./assets/background.png');
        // game.load.image('menu','./assets/menu.png');

        game.load.spritesheet('player', './assets/player.png', 32, 32);
        game.load.spritesheet('conveyRight', './assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyLeft', './assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', './assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', './assets/fake.png', 96, 36);

        // Sound: player jumps
        game.load.audio('jump', ['assets/sounds/jump.ogg']);
        game.load.audio('hit', ['assets/sounds/hit.wav']);
        game.load.audio('die', ['assets/sounds/die.wav']);
        // game.load.audio('menu_music', ['assets/sounds/menu_music.wav']);
        game.load.audio('bgm', ['assets/sounds/bgm.wav']);
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        //game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
    },
    create:function(){

    },
    update:function(){
        game.state.start('menu');
    }
};

var menuState = {
    preload:function(){},
    create:function(){
        //if (!game.device.desktop){ game.input.onDown.add(gofull, this); }
        //sound
        // menu_musicSound = game.add.audio('menu_music');
        // menu_musicSound.loop = true;
        menu_musicSound.play();
        
        game.add.tileSprite(0, 0, 2000,2000, 'menu').scale.setTo(0.5,0.5);
        //game.physics.startSystem(Phaser.Physics.ARCADE);
        //game.stage.backgroundColor = 'black'; // Set background
        var style = {fill: 'white', fontSize: '20px'};
        var title_style = {fill: 'white', fontSize: '40px'};
        this.NS = game.add.text(290, 80, 'NS-Shaft', title_style);
        this.MENU = game.add.text(320, 150, 'MENU', title_style);
        this.index = game.add.text(250, 250, 'One player', style);
        this.index2 = game.add.text(400, 250, 'Two players', style);
        this.tri = game.add.sprite(220, 245, 'tri');
        this.tri.scale.setTo(0.05,0.05);

        //player
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.player = game.add.sprite(300, 300, 'player');
        game.physics.arcade.enable(this.player);
        this.player.facingLeft = false;
        //animations
        this.player.animations.add('left', [0, 1, 2, 3], 8);
        this.player.animations.add('right', [9, 10, 11, 12], 8);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player.animations.add('fly', [36, 37, 38, 39], 12);

        this.point = game.input.addPointer();
        this.point1 = game.input.addPointer();
    
    },
    update:function(){
        game.physics.arcade.enable(this.tri, Phaser.Physics.ARCADE);
        this.cursor = game.input.keyboard.createCursorKeys();
        this.menu();
        this.moveplayer();
    },

    menu:function(){
        var keyboard2 = game.input.keyboard.addKeys({'enter': Phaser.Keyboard.ENTER});

        if(this.cursor.left.isDown || (game.input.mousePointer.x<350&&game.input.mousePointer.isDown)||(game.input.pointer1.x<350&&game.input.pointer1.isDown)) {
            this.tri.position.x = 220;
        }
        else if(this.cursor.right.isDown|| (game.input.mousePointer.x>=350&&game.input.mousePointer.isDown)||(game.input.pointer1.x>=350&&game.input.pointer1.isDown)){
            this.tri.position.x = 370;
        }
        if(keyboard2.enter.isDown || game.input.mousePointer.isDown || game.input.pointer1.isDown){
            if(this.tri.position.x == 220)
                game.state.start('main');
            else
                game.state.start('two_players');
        }
    },
    moveplayer:function(){
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
            this.player.facingLeft = true;
            if(this.player.body.velocity.y>9)
                this.player.animations.play('flyleft');
            else
                this.player.animations.play('left');
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;
            this.player.facingLeft = false;
            if(this.player.body.velocity.y>9)
                this.player.animations.play('flyright');
            else
                this.player.animations.play('right');
        }
        else {
            //console.log(this.player.body.velocity.y);
            // Stop the player 
            this.player.body.velocity.x = 0;
            if(this.player.body.velocity.y>9){
                this.player.animations.play('fly');
            }
            else{
                this.player.frame = 8;
                this.player.animations.stop();
            }
        }
    }
};

var mainState = {
    preload:function(){},
    create:function(){

        this.menu = game.add.sprite(140, 0, 'menu').scale.setTo(0.5,0.5);
        game.add.tileSprite(-10, -100, 1050,2000, 'background').scale.setTo(0.4,0.4);
        //game.stage.backgroundColor = 'black'; // Set background
        game.renderer.renderSession.roundPixels = true; // Setup renderer
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.cursor = game.input.keyboard.createCursorKeys();

        //create player
        this.player = game.add.sprite(200, 50, 'player');
        this.player.facingLeft = false;
        //animations
        this.player.animations.add('left', [0, 1, 2, 3], 8);
        this.player.animations.add('right', [9, 10, 11, 12], 8);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player.animations.add('fly', [36, 37, 38, 39], 12);
        //physics down
        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 500;
        //setting player
        this.player.life = 10;
        this.player.invincible_time = 0;
        this.player.touch = undefined;

        //create boundary
        this.left = game.add.sprite(0,0,'wall');
        this.right = game.add.sprite(400,0,'wall');
        this.left2 = game.add.sprite(0,400,'wall');
        this.right2 = game.add.sprite(400,400,'wall');
        this.ceiling = game.add.sprite(0,0,'ceiling');
        game.physics.arcade.enable(this.left);
        this.left.body.immovable = true;
        game.physics.arcade.enable(this.right);
        this.right.body.immovable = true;

        //create shaft
        //this.shaft = game.add.sprite(200,200,'shaft');
        //game.physics.arcade.enable(this.shaft);
        //this.shaft.body.immovable = true;

        //create texts
        var style = {fill: 'white', fontSize: '20px'}
        this.life = game.add.text(500, 100, '', style);
        this.score = game.add.text(500, 200, '', style);
        this.restart_enter = game.add.text(110, 100, 'Press Enter to restart', style);
        this.final_score= game.add.text(140, 140, '', style);
        this.restart_enter.visible = false;
        this.final_score.visible = false;  


        //Sounds
        menu_musicSound.stop();
        this.bgm = game.add.audio('bgm');
        this.bgm.loop = true;
        this.bgm.volume = 0.3;
        this.bgm.play();
        jumpSound = game.add.audio('jump');
        jumpSound.volume = 0.2;
        hitSound = game.add.audio('hit');
        dieSound = game.add.audio('die');
        dieSound.volume = 0.3;

        this.point = game.input.addPointer();

        last_shaft_time = game.time.now;
        game.global.player_name = prompt("Please enter your name", "name");
        if(game.global.player_name == undefined)
            game.state.start('menu');
    },
    
    update:function(){
        var keyboard2 = game.input.keyboard.addKeys({'enter': Phaser.Keyboard.ENTER});

        
        //if(status != 'running') return;

        game.physics.arcade.collide(this.player, [this.left,this.left2,this.right,this.right2]);
        game.physics.arcade.collide(this.player, shafts, effect);

        this.life.setText('life:'+this.player.life);
        this.score.setText('score:'+score);

        this.moveplayer();
        this.update_shafts();
        create_shaft();
        //this.effect();
        touchCeiling(this.player);
        if(status != 'gameover')   this.gameover();
        if(status == 'gameOver' && (keyboard2.enter.isDown||game.input.mousePointer.isDown||game.input.pointer1.isDown)) this.restart();
    },

    moveplayer:function(){
        if (this.cursor.left.isDown || (game.input.mousePointer.x<200&&game.input.mousePointer.isDown)||(game.input.pointer1.x<200&&game.input.pointer1.isDown)) {
            this.player.body.velocity.x = -200;
            this.player.facingLeft = true;
            if(this.player.body.velocity.y>9)
                this.player.animations.play('flyleft');
            else
                this.player.animations.play('left');
        }
        else if (this.cursor.right.isDown || (game.input.mousePointer.x>=200&&game.input.mousePointer.isDown)||(game.input.pointer1.x>=200&&game.input.pointer1.isDown)) { 
            this.player.body.velocity.x = 200;
            this.player.facingLeft = false;
            if(this.player.body.velocity.y>9)
                this.player.animations.play('flyright');
            else
                this.player.animations.play('right');
        }
        else {
            //console.log(this.player.body.velocity.y);
            // Stop the player 
            this.player.body.velocity.x = 0;
            if(this.player.body.velocity.y>9){
                this.player.animations.play('fly');
            }
            else{
                this.player.frame = 8;
                this.player.animations.stop();
            }
        }
    },

    update_shafts:function(){
        for(var i=0; i<shafts.length; i++) {
            this.shaft = shafts[i];
            game.physics.arcade.enable(this.shaft);
            game.physics.arcade.collide(this.player, this.shaft);
            this.shaft.body.position.y -= 2;
            //this.effect(this.shaft);
            if(this.shaft.body.position.y <= -20) {
                this.shaft.destroy();
                shafts.splice(i, 1);
            }
        }
    },

    gameover:function(){
        if((this.player.life <= 0 || this.player.body.y > 500)&&status != 'gameOver'){
            dieSound.play();
            status = 'gameOver';
            this.restart_enter.visible = true;
            this.final_score.setText('Your score :' + score);
            this.final_score.visible = true;
            shafts.forEach(function(t) {t.destroy()});
            shafts = [];
            //status = 'gameOver';
            //this.player.body.enable = false;
            this.player.body.collideWorldBounds = false;
            this.player.body.checkCollision.down = false;
            this.player.body.checkCollision.up = false;
            last_shaft_time += 800;
            //firebase 
            var Ref = firebase.database().ref('score_list');
            console.log(game.global.player_name);
            var data = {
                name: game.global.player_name,
                score : 0-score
            }
            Ref.push(data);
            var style = {fill: 'white', fontSize: '20px'};
            this.rank = game.add.text(170, 200, 'Ranking', style);

            var Rank = firebase.database().ref('score_list').orderByChild('score').limitToFirst(3);

            Rank.once('value').then(function (childshot) {
                var n = 0;
                childshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    game.global.rank_player[n] = childData.name;
                    game.global.rank_score[n] = 0 - childData.score;
                    //console.log(n);
                    //console.log(game.global.rank_player[n]);
                    //console.log(game.global.rank_score[n]);
                    n+=1;
                });

                for(var i=0;i<3;i++){
                    ranking[i] = game.add.text(170, 230+30*i, game.global.rank_player[i]+':'+game.global.rank_score[i], style);
                }
            });
            // for(var i=0;i<3;i++){
            //     console.log(game.global.rank_player[i]);
            //     console.log(game.global.rank_score[i]);
            // }
        }
    },
    restart:function(){
        this.bgm.stop();
        score = 0;
        score2 = 0;
        last_shaft_time = 0;
        status = 'running';
        game.state.start('menu');
    }
};

var two_playerState = {
    preload:function(){
        game.load.spritesheet('player2', './assets/player2.png', 32, 32);
    },
    create:function(){

        this.menu = game.add.sprite(140, 0, 'menu').scale.setTo(0.5,0.5);
        game.add.tileSprite(-10, -100, 1050,2000, 'background').scale.setTo(0.4,0.4);
        //game.stage.backgroundColor = 'black'; // Set background
        game.renderer.renderSession.roundPixels = true; // Setup renderer
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.cursor = game.input.keyboard.createCursorKeys();

        //create player
        this.player = game.add.sprite(300, 50, 'player');
        this.player2 = game.add.sprite(100, 50, 'player2');
        this.player.facingLeft = false;
        this.player2.facingLeft = false;
        //animations
        this.player.animations.add('left', [0, 1, 2, 3], 8);
        this.player.animations.add('right', [9, 10, 11, 12], 8);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player.animations.add('fly', [36, 37, 38, 39], 12);
        this.player2.animations.add('left', [0, 1, 2, 3], 8);
        this.player2.animations.add('right', [9, 10, 11, 12], 8);
        this.player2.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player2.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player2.animations.add('fly', [36, 37, 38, 39], 12);
        //physics down
        game.physics.arcade.enable(this.player);
        game.physics.arcade.enable(this.player2);
        this.player.body.gravity.y = 500;
        this.player2.body.gravity.y = 500;
        //setting player
        this.player.life = 10;
        this.player.invincible_time = 0;
        this.player.touch = undefined;
        this.player2.life = 10;
        this.player2.invincible_time = 0;
        this.player2.touch = undefined;

        //create boundary
        this.left = game.add.sprite(0,0,'wall');
        this.right = game.add.sprite(400,0,'wall');
        this.left2 = game.add.sprite(0,400,'wall');
        this.right2 = game.add.sprite(400,400,'wall');
        this.ceiling = game.add.sprite(0,0,'ceiling');
        game.physics.arcade.enable(this.left);
        this.left.body.immovable = true;
        game.physics.arcade.enable(this.right);
        this.right.body.immovable = true;

        //create shaft
        //this.shaft = game.add.sprite(200,200,'shaft');
        //game.physics.arcade.enable(this.shaft);
        //this.shaft.body.immovable = true;

        //create texts
        var style = {fill: 'white', fontSize: '20px'}
        this.life = game.add.text(500, 100, '', style);
        this.score = game.add.text(500, 200, '', style);
        this.life2 = game.add.text(500, 130, '', style);
        this.score2 = game.add.text(500, 230, '', style);
        this.restart_enter = game.add.text(110, 100, 'Press Enter to restart', style);
        //this.final_score= game.add.text(140, 140, '', style);
        this.winner = game.add.text(110, 140, '', style);
        this.restart_enter.visible = false;
        this.winner.visible = false;

        this.point = game.input.addPointer();

        //sound
        menu_musicSound.stop();
        jumpSound = game.add.audio('jump');
        jumpSound.volume = 0.5;
        hitSound = game.add.audio('hit');
        dieSound = game.add.audio('die');
        dieSound.volume = 0.5;
        this.bgm = game.add.audio('bgm');
        menu_musicSound.stop();
        this.bgm.loop = true;
        this.bgm.volume = 0.3;
        this.bgm.play();

        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D
        });
    },
    

    update:function(){

        if(status == 'gameOver' && (keyboard.enter.isDown||game.input.mousePointer.isDown||game.input.pointer1.isDown)) this.restart();
        //if(status != 'running') return;

        game.physics.arcade.collide([this.player,this.player2], [this.left,this.left2,this.right,this.right2]);
        //game.physics.arcade.collide(this.player, this.right);
        game.physics.arcade.collide([this.player,this.player2], shafts, effect);
        game.physics.arcade.collide(this.player, this.player2);

        this.life.setText('life:'+this.player.life);
        this.life2.setText('life:'+this.player2.life);
        this.score.setText('score:'+score);
        this.score2.setText('score:'+score2);

        this.moveplayer();
        this.update_shafts();
        create_shaft();
        //this.effect();
        touchCeiling(this.player);
        touchCeiling(this.player2);
        //if(status!= 'gameOver')
            this.gameover();
    },

    moveplayer:function(){
        // player 1
        if (this.cursor.left.isDown ) {
            this.player.body.velocity.x = -200;
            this.player.facingLeft = true;
            if(this.player.body.velocity.y>9)
                this.player.animations.play('flyleft');
            else
                this.player.animations.play('left');
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;
            this.player.facingLeft = false;
            if(this.player.body.velocity.y>9)
                this.player.animations.play('flyright');
            else
                this.player.animations.play('right');
        }
        else {
            //console.log(this.player.body.velocity.y);
            // Stop the player 
            this.player.body.velocity.x = 0;
            if(this.player.body.velocity.y>9){
                this.player.animations.play('fly');
            }
            else{
                this.player.frame = 8;
                this.player.animations.stop();
            }
        }
        //player 2 
        if (keyboard.a.isDown) {
            this.player2.body.velocity.x = -200;
            this.player2.facingLeft = true;
            if(this.player2.body.velocity.y>9)
                this.player2.animations.play('flyleft');
            else
                this.player2.animations.play('left');
        }
        else if (keyboard.d.isDown) { 
            this.player2.body.velocity.x = 200;
            this.player2.facingLeft = false;
            if(this.player2.body.velocity.y>9)
                this.player2.animations.play('flyright');
            else
                this.player2.animations.play('right');
        }
        else {
            //console.log(this.player.body.velocity.y);
            // Stop the player 
            this.player2.body.velocity.x = 0;
            if(this.player2.body.velocity.y>9){
                this.player2.animations.play('fly');
            }
            else{
                this.player2.frame = 8;
                this.player2.animations.stop();
            }
        }
    },

    update_shafts:function(){
        for(var i=0; i<shafts.length; i++) {
            this.shaft = shafts[i];
            game.physics.arcade.enable(this.shaft);
            game.physics.arcade.collide(this.player, this.shaft);
            game.physics.arcade.collide(this.player2, this.shaft);
            this.shaft.body.position.y -= 2;
            //this.effect(this.shaft);
            if(this.shaft.body.position.y <= -20) {
                this.shaft.destroy();
                shafts.splice(i, 1);
            }
        }
    },

    gameover:function(){
        if(this.player.life <= 0 || this.player.body.y > 600) {
            this.winner.setText('The Winner is Player 2');
            this.restart_enter.visible = true;
            this.winner.visible = true;
            shafts.forEach(function(s) {s.destroy()});
            shafts = [];
            status = 'gameOver';
        }
        else if(this.player2.life <= 0 || this.player2.body.y > 600) {
            this.winner.setText('The Winner is Player 1');
            this.restart_enter.visible = true;
            this.winner.visible = true;
            shafts.forEach(function(s) {s.destroy()});
            shafts = [];
            status = 'gameOver';
        }
    },
    restart:function(){
        this.bgm.stop();
        score = 0;
        score2 = 0;
        last_shaft_time = 0;
        status = 'running';
        game.state.start('menu');
        //this.create();
    }
};

function touchCeiling(player){
    //console.log(this.player.body.y);
    if(player.body.y<0){
        if(player.body.velocity.y<0)
            player.body.velocity.y =0;
        if(game.time.now > player.invincible_time){
            player.life -= 3;
            game.camera.flash(0xff0000, 100);
            game.camera.shake(0.02, 300);
            player.invincible_time = game.time.now + 2000;
            hitSound.play();
        }
    }
}

function create_shaft(){
    if(game.time.now > last_shaft_time+800){
        //console.log(last_shaft_time);
        last_shaft_time = game.time.now;
        create_random_shaft();
        if(status=='running'){
            score += 1;
            score2 += 1;
        }
    }
}

function create_random_shaft(){

    var shaft_random
    var x = Math.random()*(400 - 96 - 40) + 20;
    var y = 500;
    var rand = Math.random() * 100;

    if(rand<20){
        shaft_random = game.add.sprite(x,y,'shaft');
    }
    else if(rand<40){
        shaft_random = game.add.sprite(x,y,'spiking');
        game.physics.arcade.enable(shaft_random);
        shaft_random.body.setSize(96, 15, 0, 15);
    }
    else if(rand<50){
        shaft_random = game.add.sprite(x,y,'conveyRight');
        shaft_random.animations.add('roll',[0,1,2,3],16,true);
        shaft_random.play('roll');
    }
    else if(rand<60){
        shaft_random = game.add.sprite(x,y,'conveyLeft');
        shaft_random.animations.add('roll',[0,1,2,3],16,true);
        shaft_random.play('roll');
    }
    else if(rand<80){
        shaft_random = game.add.sprite(x,y,'trampoline');
        shaft_random.animations.add('jump',[4,5,4,3,2,1,0,1,2,3],120);
        //shaft_random.play('jump');
        shaft_random.frame = 3;
    }
    else{
        shaft_random = game.add.sprite(x,y,'fake');
        shaft_random.animations.add('turn',[0,1,2,3,4,5,0],14);
        //shaft_random.play('turn');
    }

    game.physics.arcade.enable(shaft_random);
    shaft_random.body.checkCollision.down = false;
    shaft_random.body.checkCollision.left = false;
    shaft_random.body.checkCollision.right = false;
    shaft_random.body.immovable = true;
    shafts.push(shaft_random);
}

//show different on the shaft
function effect(player,shaft_effect){
    //console.log(shaft_effect.key);
    if(shaft_effect.key == 'spiking'){
        if(player.touch!=shaft_effect.key){
            player.life -= 3;   
            game.camera.flash(0xff0000, 100);
            game.camera.shake(0.02, 300);
            player.touch = shaft_effect.key;
            hitSound.play();
            //console.log(player.touch);
        } 
    }
    else if(shaft_effect.key == 'conveyRight'){
        player.body.x += 2;
        player.touch = shaft_effect.key;
    }
    else if(shaft_effect.key == 'conveyLeft'){
        player.body.x -= 2;
        player.touch = shaft_effect.key;
    }
    else if(shaft_effect.key == 'trampoline'){
        shaft_effect.animations.play('jump');
        jumpSound.play();
        player.body.velocity.y = -300;
        player.touch = shaft_effect.key;
    }
    else if(shaft_effect.key == 'fake'){
        shaft_effect.animations.play('turn');
        setTimeout(function() {
            shaft_effect.body.checkCollision.up = false;
        }, 100);
        player.touch = shaft_effect.key;
    }
    else if(shaft_effect.key == 'shaft'){
        if(player.touch!=shaft_effect.key)    
            if(player.life<10)
                player.life +=1;
        player.touch = shaft_effect.key;
    }
    else{
        player.touch = undefined;
    }
}

var game = new Phaser.Game(800, 500, Phaser.AUTO, 'canvas');
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('main', mainState);
game.state.add('two_players', two_playerState);
game.state.start('boot');
game.global ={
    player_name : undefined,
    rank_player : [],
    rank_score : []
}
// game.global.player_name = undefined;
// game.global.rank_player = undefined;
// game.global.rank_score = 0;